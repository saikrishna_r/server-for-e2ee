package main

import (
"database/sql"
"fmt"
"net/http"
"time"

"github.com/gin-gonic/gin"
_ "github.com/go-sql-driver/mysql"
"golang.org/x/crypto/pbkdf2"
"github.com/itsjamie/gin-cors"
"encoding/base64"
"crypto/sha1"
"github.com/satori/go.uuid"
"path/filepath"
"os"
//"io"
"io/ioutil"
)


func main() {
	router:=gin.Default()

	router.Use(cors.Middleware(cors.Config{
    Origins:        "*",
    Methods:        "GET, PUT, POST, DELETE",
    RequestHeaders: "Origin, Authorization, Content-Type",
    ExposedHeaders: "",
    MaxAge: 50 * time.Second,
    Credentials: true,
    ValidateHeaders: false,
}))

	database,_ :=sql.Open("mysql","krishnapt1251:password@/E2EE")
	defer database.Close()

	router.POST("/registeruser",func(c *gin.Context) {


		var st string

		userName:=c.PostForm("userName")
		salt:=c.PostForm("salt")
		userKey:=c.PostForm("userKey")

		row:=database.QueryRow("SELECT SALT FROM USER_CREDENTIALS WHERE USERNAME=?",userName)
		row.Scan(&st)

		if st!="" {
			c.JSON(401,gin.H{
				"message":"User Already exists Please try a different Username",
				})
		} else {



			stmt,_ :=database.Prepare("INSERT INTO USER_CREDENTIALS VALUES(?,?,?)");
			stmt.Exec(userName,userKey,salt);


			c.JSON(http.StatusOK,gin.H{
				"message":fmt.Sprintf("Registered User %s Successfully",userName),
				})

		}
		})

	router.POST("/authorizeuser",func(c *gin.Context) {

		var userKey string
		var salt string
		//var decodedSalt string

		userName:=c.PostForm("userName")
		hashedPassword:=c.PostForm("hashedPassword")

		fmt.Println("Username",userName)
		fmt.Println("hashedPassword:",hashedPassword)

		row:=database.QueryRow("SELECT USERKEY,SALT FROM USER_CREDENTIALS WHERE USERNAME=?;",userName);
		row.Scan(&userKey,&salt);

		fmt.Println("Salt:",salt)
		decodedSalt,_:=base64.StdEncoding.DecodeString(salt)

		derivedKey := pbkdf2.Key([]byte(hashedPassword), decodedSalt, 5000, 32, sha1.New)

		encodedDerivedKey :=base64.StdEncoding.EncodeToString(derivedKey)

		fmt.Println("Derived Key:",encodedDerivedKey)
		fmt.Println("User Key:",userKey)

		if encodedDerivedKey==userKey {
			UUID:=uuid.NewV4()
			c.JSON(http.StatusOK,gin.H{
				"message":fmt.Sprintf("User Authenticated"),
				"uuid":UUID,
				})
			stmt,_:=database.Prepare("INSERT INTO LOGGED_IN_USER VALUES(?,?)");
			stmt.Exec(userName,UUID)
		} else {
			c.JSON(401,gin.H{
				"message":fmt.Sprintf("User Authentication Failed"),
				})
		}


		})

	router.POST("/logout",func(c *gin.Context) {
		userName:=c.PostForm("userName")

		stmt,err:=database.Prepare("DELETE FROM LOGGED_IN_USER WHERE USERNAME=?");
		checkError(err)
		stmt.Exec(userName)
		checkError(err)

		c.JSON(200,gin.H{
			"message":fmt.Sprintf("User Logged Out Successfully"),
			})

		})

	router.POST("/openfileList",func(c *gin.Context) {

		var fileNames[] string
		var storedUuid string
		var fName string

		userName:=c.PostForm("userName")
		UUID:=c.PostForm("uuid")

		row:=database.QueryRow("SELECT UUID FROM LOGGED_IN_USER WHERE USERNAME=?",userName)
		row.Scan(&storedUuid)

		if UUID==storedUuid {

			stmt,err := database.Prepare("SELECT FILENAME FROM ENDTOEND WHERE USERNAME= ?;")
			rows,err :=stmt.Query(userName)
			checkError(err)
			defer rows.Close()

			for rows.Next() {
				err := rows.Scan(&fName)
				fileNames=append(fileNames,fName)
				checkError(err)


			}

			c.JSON(200,gin.H{
				"message":"Request Success",
				"fileList":fileNames,

				})
		} else {
			c.JSON(401,gin.H {
				"message":"Request Failed",
				})
		}

		})

	router.POST("/openFile",func(c *gin.Context) {

		var storedUuid string
		var hKey string
		var iv string
		var st string
		var eKey string
		var fileText string
		var fPath string

		userName:=c.PostForm("userName")
		UUID:=c.PostForm("uuid")
		fileName:=c.PostForm("fileName")

		row:=database.QueryRow("SELECT UUID FROM LOGGED_IN_USER WHERE USERNAME=?",userName);
		row.Scan(&storedUuid)

		if storedUuid==UUID {
			stmt,err := database.Prepare("SELECT HALFKEY,INITVECTOR,SALT,ENCRYPTEDKEY,filepath FROM ENDTOEND WHERE USERNAME= ? AND FILENAME= ? ;")
			stmt.QueryRow(userName,fileName).Scan(&hKey,&iv,&st,&eKey,&fPath)
			checkError(err)


			path := fPath
			file,err := ioutil.ReadFile(path)
			checkError(err)
			fileText = string(file)


			c.JSON(200,gin.H {
				"message":"File Opened Successfully",
				"halfKey":hKey,
				"initVector":iv,
				"salt":st,
				"encryptedKey":eKey,
				"encryptedFile":fileText,
				"filePath":fPath,
				})
		} else {
			c.JSON(401,gin.H {
				"message":"Open File Failed",
				})
		}


		})


	router.POST("/newFile",func(c *gin.Context) {

		var storedUuid string

		userName:=c.PostForm("userName")
		UUID:=c.PostForm("uuid")
		fileName:=c.PostForm("fileName")
		encryptedFileText:=c.PostForm("encryptFileText")
		saltText:=c.PostForm("saltText")
		initVectorText:=c.PostForm("initVectorText")
		userPasswordKeySecondHalf:=c.PostForm("halfKey")
		encryptedKeyText:=c.PostForm("encryptedKeyText")


		row:=database.QueryRow("SELECT UUID FROM LOGGED_IN_USER WHERE USERNAME=?",userName);
		row.Scan(&storedUuid)

		if storedUuid==UUID {
			stmt,_:=database.Prepare("INSERT INTO ENDTOEND(USERNAME,HALFKEY,INITVECTOR,FILENAME,SALT,ENCRYPTEDKEY) VALUES(?,?,?,?,?,?)")
			stmt.Exec(userName,userPasswordKeySecondHalf,initVectorText,fileName,saltText,encryptedKeyText)
			if _,err := os.Stat(userName); os.IsNotExist(err) {
				os.MkdirAll(userName,0777)
			}

			path := filepath.Join(userName,fileName)


			file,err := os.Create(path)

			checkError(err)

			defer file.Close()

			_,err = file.WriteString(encryptedFileText)

			stmt,_=database.Prepare("UPDATE ENDTOEND SET filepath=? WHERE USERNAME=? AND FILENAME=?")
			stmt.Exec(path,userName,fileName)
			checkError(err)

			file.Sync()

			c.JSON(200,gin.H{
				"message":"Upload Successful",
				"filePath":path,
				})

		} else {
			c.JSON(401,gin.H{
				"message":"Upload Failed",
				})

		}


		})


	router.POST("/saveNewPassword",func(c *gin.Context){

		var storedUuid string

		userName:=c.PostForm("userName")
		UUID:=c.PostForm("uuid")
		fileName:=c.PostForm("fileName")
		//encryptedFileText:=c.PostForm("encryptFileText")
		saltText:=c.PostForm("salt")
		initVectorText:=c.PostForm("initVector")
		userPasswordKeySecondHalf:=c.PostForm("halfKey")
		encryptedKeyText:=c.PostForm("encryptedFileKey")
		filepath:=c.PostForm("filePath")
		aad:=c.PostForm("aad")


		row:=database.QueryRow("SELECT UUID FROM LOGGED_IN_USER WHERE USERNAME=?",userName);
		row.Scan(&storedUuid)

		if storedUuid==UUID {
			stmt,_:=database.Prepare("DELETE FROM ENDTOEND WHERE USERNAME=? AND FILENAME=?");
			stmt.Exec(userName,fileName)
			stmt1,_:=database.Prepare("INSERT INTO ENDTOEND VALUES(?,?,?,?,?,?,?,?)")
			stmt1.Exec(userName,userPasswordKeySecondHalf,initVectorText,fileName,saltText,encryptedKeyText,filepath,aad)

			c.JSON(200,gin.H{
				"message":"Password Changed Successfully",
				})

			} else
			{
				c.JSON(401,gin.H{
					"message":"Error!",
					})
			}


			})

	router.POST("/shareCredentials",func(c *gin.Context) {

		var storedUuid string

		userName:=c.PostForm("userName")
		shareUserName:=c.PostForm("shareUserName")
		UUID:=c.PostForm("uuid")
		fileName:=c.PostForm("fileName")
		//encryptedFileText:=c.PostForm("encryptFileText")
		saltText:=c.PostForm("salt")
		initVectorText:=c.PostForm("initVector")
		userPasswordKeySecondHalf:=c.PostForm("halfKey")
		encryptedKeyText:=c.PostForm("encryptedFileKey")
		filepath:=c.PostForm("filePath")
		aad:=c.PostForm("aad")


		row:=database.QueryRow("SELECT UUID FROM LOGGED_IN_USER WHERE USERNAME=?",userName);
		row.Scan(&storedUuid)

		if storedUuid==UUID {
			stmt1,_:=database.Prepare("INSERT INTO ENDTOEND VALUES(?,?,?,?,?,?,?,?)")
			stmt1.Exec(shareUserName,userPasswordKeySecondHalf,initVectorText,fileName,saltText,encryptedKeyText,filepath,aad)

			c.JSON(200,gin.H{
				"message":"Password Changed Successfully",
				})

			} else
			{
				c.JSON(401,gin.H{
					"message":"Error!",
					})
			}

			})

	router.POST("/checkFileName",func (c *gin.Context) {

		var hKey string
		var storedUuid string
		hKey=""
		userName:=c.PostForm("userName")
		UUID:=c.PostForm("uuid")
		fileName:=c.PostForm("fileName")

		row:=database.QueryRow("SELECT UUID FROM LOGGED_IN_USER WHERE USERNAME=?",userName);
		row.Scan(&storedUuid)
		fmt.Println(storedUuid)
		fmt.Println(UUID)

		if storedUuid==UUID {
			stmt,_:=database.Prepare("SELECT HALFKEY FROM ENDTOEND WHERE USERNAME=? AND FILENAME=?")
			row:=stmt.QueryRow(userName,fileName)
			row.Scan(&hKey)

			if hKey!="" {
				c.JSON(200,gin.H{
					"message":"File Already Exists",
					})
			} else {
				c.JSON(401,gin.H{
					"message":"File Doesnt Exist",
					})
			}
		} else {
			c.JSON(404,gin.H{
				"message":"Failure!!",
				})
		}




		})




	router.Run(":2900")




}


func checkError(err error) {
	if err != nil {
		fmt.Fprintf(os.Stderr, "Fatal error: %s", err.Error())
		os.Exit(1)
	}
}
