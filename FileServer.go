package main

import (
"os"
"net"
"fmt"
//"strings"
"database/sql"
	_ "github.com/go-sql-driver/mysql"
"path/filepath"
"encoding/json"
"log"	



)


type ShareDetails struct {
    userName string
    halfKey string
    initVector string
    fileName string
    salt string
    encryptedKey string
}

type Userdata struct {
    UserKey string `json:"userKey"`
    Salt string `json:"salt"`
}

type UserDetails struct {
    userName string
    halfKey string
    initVector string
    fileName string
    salt string
    encryptedKey string
    encryptedFile string
}


type Response struct {
    
    Halfkey string `json:"halfKey"`
    Initvector string `json:"initVector"`
    Salt string `json:"salt"`
    Encryptedkey string `json:"encryptedKey"`
    Encryptedfile string  `json:"encryptedFile"`

}

var hKey string
var iv string
var st string
var eKey string
var fileText string

type UserFiles struct {
    FileArray []string `json:"fileArray"`
}


var fileNames[] string
var fName string




func registerUserPort() {
    service:=":2700"
    tcpAddr, err := net.ResolveTCPAddr("tcp",service)

    checkError(err)

    listener,err := net.ListenTCP("tcp",tcpAddr)

    checkError(err)

    for {
        conn , err :=listener.Accept()
            if err != nil {
                continue
            }
            
            go registerUser(conn)

        
    }
}



func changePasswordPort() {
    service := ":2500"

    tcpAddr, err := net.ResolveTCPAddr("tcp",service)

    checkError(err)

    listener,err := net.ListenTCP("tcp",tcpAddr)

    checkError(err)

    for {
        conn , err :=listener.Accept()
            if err != nil {
                continue
            }
            
            go changePassword(conn)

        
    }
}

func shareFilePort() {
    service := ":2000"

    tcpAddr, err := net.ResolveTCPAddr("tcp",service)

    checkError(err)

    listener,err := net.ListenTCP("tcp",tcpAddr)

    checkError(err)

    for {
        conn , err :=listener.Accept()
            if err != nil {
                continue
            }
            
            go shareFile(conn)

        
    }
}







func createFilePort() {
    service := ":1200"



    tcpAddr, err := net.ResolveTCPAddr("tcp",service)

    checkError(err)

    listener,err := net.ListenTCP("tcp",tcpAddr)

    checkError(err)

    for {
        conn , err :=listener.Accept()
            if err != nil {
                continue
            }
            
            go createFile(conn)

        
    }
}

func authorizeUserPort() {
    service:=":2701"
    tcpAddr, err := net.ResolveTCPAddr("tcp",service)

    checkError(err)

    listener,err := net.ListenTCP("tcp",tcpAddr)

    checkError(err)

    for {
        conn , err :=listener.Accept()
            if err != nil {
                continue
            }
            
            go authenticateUser(conn)

        
    }
}


func openFilePort() {
    service := ":1500"

    
    tcpAddr, err := net.ResolveTCPAddr("tcp",service)

    checkError(err)

    listener,err := net.ListenTCP("tcp",tcpAddr)

    checkError(err)

    for {
        conn , err :=listener.Accept()
            if err != nil {
                continue
            }
            
            go openFile(conn)

        
    }

}

func openFilePort2() {
    service := ":2501"

    
    tcpAddr, err := net.ResolveTCPAddr("tcp",service)

    checkError(err)

    listener,err := net.ListenTCP("tcp",tcpAddr)

    checkError(err)

    for {
        conn , err :=listener.Accept()
            if err != nil {
                continue
            }
            
            go openFile2(conn)

        
    }
}


func createFile (conn net.Conn) {

	database,err :=sql.Open("mysql","krishnapt1251:password@/E2EE")
	if err != nil {
		panic(err.Error())
		}

	defer conn.Close()
	defer database.Close()

	var buf [2048] byte
	//var fileNameBuffer [512] byte

        // read upto 512 bytes

		//n,err :=conn.Read(fileNameBuffer[0:])

        n1, err := conn.Read(buf[0:])

        //fileName := string(fileNameBuffer[:n])
        //fileName+=".txt"

        s :=string(buf[:n1])

        fmt.Println(s)

        var userData map[string]interface{}

        if err := json.Unmarshal([]byte(s),&userData); err!=nil {
            panic(err)
        }

        fmt.Println("JSON OBJECT :",userData)

        


        //text := strings.Split(s,"\n")

        

        userName := userData["userName"].(string)
        halfKey := userData["userPasswordKey"].(string)

        initVector:=userData["initVector"].(string)

        fileName :=userData["fileName"].(string)

        salt := userData["salt"].(string)

        encryptedKey:= userData["encryptedKey"].(string)

        encryptedFile := userData["encryptedFile"].(string)


        fmt.Println("Username:",userName)
        fmt.Println("Half Key:",halfKey)
        fmt.Println("Init Vector:",initVector)
        fmt.Println("File name :",fileName)
        fmt.Println("Salt:",salt)
        fmt.Println("Encrypted Key :",encryptedKey)
        fmt.Println("Encrypted File:",encryptedFile)


            
        stmtIn,err:=database.Prepare("INSERT INTO ENDTOEND VALUES(?,?,?,?,?,?)")

        stmtIn.Exec(userName,halfKey,initVector,fileName,salt,encryptedKey)

        checkError(err)

        if _,err := os.Stat(userName); os.IsNotExist(err) {
        	os.MkdirAll(userName,0777)
        }

        path := filepath.Join(userName,fileName)
       

        file,err := os.Create(path)

        checkError(err)

        defer file.Close()

        n2,err := file.WriteString(encryptedFile)

        checkError(err)

        fmt.Println("Wrote %d bytes ",n2)


        file.Sync()

        checkError(err)
        
        fmt.Println(s)


        

        // write the n bytes read
        //_, err2 := conn.Write(buf[0:n])
        //if err2 != nil {
          //  return
        //}
    

			

}



func authenticateUser(conn net.Conn) {
    database,err :=sql.Open("mysql","krishnapt1251:password@/E2EE")
    if err != nil {
        panic(err.Error())
        }

    defer conn.Close()
    defer database.Close()

    var userKey string
    var salt string
    var userResponse Userdata

    userResponse.UserKey=""
    userResponse.Salt=""

    var buf[2048] byte

    n1, err := conn.Read(buf[0:])
    checkError(err)

    s :=string(buf[:n1])

    var userCredentials map[string]interface{}

    salt=""
    userKey=""

    if err := json.Unmarshal([]byte(s),&userCredentials); err!=nil {
            panic(err)
        }


        userName:=userCredentials["userName"].(string)

        stmt,err :=database.Prepare("SELECT USERKEY,SALT FROM USER_CREDENTIALS WHERE USERNAME=?");
        stmt.QueryRow(userName).Scan(&userKey,&salt);
        checkError(err)


        userResponse.UserKey=userKey
        userResponse.Salt=salt


        sendData,_:=json.Marshal(&userResponse)

        _,err=conn.Write(sendData[:])

        userResponse.UserKey=""
        userResponse.Salt=""
        salt=""
        userKey=""

        fmt.Println("Wrote Response")
}



func openFile(conn net.Conn) {

    database,err :=sql.Open("mysql","krishnapt1251:password@/E2EE")
    if err != nil {
        panic(err.Error())
        }

    defer conn.Close()
    defer database.Close()

    var buf[2048] byte

    n1, err := conn.Read(buf[0:])
    checkError(err)

    s :=string(buf[:n1])

    //text := strings.Split(s,";")

    var userData map[string]interface{}
    var responseData Response
                
                responseData.Halfkey=""
                responseData.Initvector=""
                responseData.Salt=""
                responseData.Encryptedkey=""
                responseData.Encryptedfile=""
                hKey="null"
                iv="null"
                st="null"
                eKey="null"
                fileText="null"

    if err := json.Unmarshal([]byte(s),&userData); err!=nil {
            panic(err)
        }

    

        userName := userData["userName"].(string)
        fileName:= userData["fileName"].(string)
        

        fmt.Println("Username:",userName)
        fmt.Println("Filename:",fileName)

        

        stmt,err := database.Prepare("SELECT HALFKEY,INITVECTOR,SALT,ENCRYPTEDKEY FROM ENDTOEND WHERE USERNAME= ? AND FILENAME= ? ;")    
        stmt.QueryRow(userName,fileName).Scan(&hKey,&iv,&st,&eKey)
        //stmt2,err :=database.Prepare("SELECT * FROM VIRTUAL_VIEW WHERE FILENAME=?")
        //stmt.QueryRow(userName,fileName).Scan(&hKey,&iv,&st,&eKey)
    

        //defer rows.Close()

        //for rows.Next() {
            //err := rows.Scan(&hKey,&iv,&st,&eKey)
            fmt.Println("The hkey and Init Vector is :",hKey,iv)
            //if err != nil {
        //log.Fatal(err)
    //}
      //  }


            if _,err := os.Stat(userName); os.IsNotExist(err) {
                fmt.Println("Directory doesn't Exist") 
            } else {
                path := filepath.Join(userName,fileName)
                file,err := os.Open(path)
                var fileContents[2048] byte
                n1,err = file.Read(fileContents[0:])
                checkError(err)

                fileText=string(fileContents[:n1])
            }
                
                
                responseData.Halfkey=hKey
                responseData.Initvector=iv
                responseData.Salt=st
                responseData.Encryptedkey=eKey
                responseData.Encryptedfile=fileText
                
                fmt.Println("HalfKey is :",responseData.Halfkey)
                fmt.Println("Init Vector is :",responseData.Initvector)
                

                sendData,_:=json.Marshal(&responseData)
                fmt.Println(string(sendData))

                _,err=conn.Write(sendData[:])
                fmt.Println("Wrote the response back")
                responseData.Halfkey="null"
                responseData.Initvector="null"
                responseData.Salt="null"
                responseData.Encryptedkey="null"
                responseData.Encryptedfile="null"
                hKey="null"
                iv="null"
                st="null"
                eKey="null"
                fileText="null"

                checkError(err)
            
        
      

}



func openFile2(conn net.Conn) {

    database,err :=sql.Open("mysql","krishnapt1251:password@/E2EE")
    if err != nil {
        panic(err.Error())
        }

    defer conn.Close()
    defer database.Close()

    var buf[2048] byte

    n1, err := conn.Read(buf[0:])
    checkError(err)

    s :=string(buf[:n1])

    //text := strings.Split(s,";")

    var userData map[string]interface{}
    var responseData Response
    var fileList UserFiles
                
                responseData.Halfkey=""
                responseData.Initvector=""
                responseData.Salt=""
                responseData.Encryptedkey=""
                responseData.Encryptedfile=""
                hKey="null"
                iv="null"
                st="null"
                eKey="null"
                fileText="null"

    if err := json.Unmarshal([]byte(s),&userData); err!=nil {
            panic(err)
        }

    

        userName := userData["userName"].(string)
        //fileName:= userData["fileName"].(string)
        

        fmt.Println("Username:",userName)
        //fmt.Println("Filename:",fileName)

        

        stmt,err := database.Prepare("SELECT FILENAME FROM ENDTOEND WHERE USERNAME= ?;")    
        rows,err :=stmt.Query(userName)
        //stmt2,err :=database.Prepare("SELECT * FROM VIRTUAL_VIEW WHERE FILENAME=?")
        //stmt.QueryRow(userName,fileName).Scan(&hKey,&iv,&st,&eKey)
    

        defer rows.Close()

        for rows.Next() {
            err := rows.Scan(&fName)
            fileNames=append(fileNames,fName)
            
            if err != nil {
        log.Fatal(err)
    }
        }

        fmt.Println(fileNames)

        fileList.FileArray=fileNames
        sendFileData,_:=json.Marshal(&fileList)

        conn.Write(sendFileData[:])
        fileNames=fileNames[:0]
        

        
      

}

func shareFile (conn net.Conn) {

    database,err :=sql.Open("mysql","krishnapt1251:password@/E2EE")
    if err != nil {
        panic(err.Error())
        }

    defer conn.Close()
    defer database.Close()

    var buf [2048] byte
    //var fileNameBuffer [512] byte

        // read upto 512 bytes

        //n,err :=conn.Read(fileNameBuffer[0:])

        n1, err := conn.Read(buf[0:])

        //fileName := string(fileNameBuffer[:n])
        //fileName+=".txt"

        s :=string(buf[:n1])

        fmt.Println(s)

        var shareUserData map[string]interface{}

        if err := json.Unmarshal([]byte(s),&shareUserData); err!=nil {
            panic(err)
        }

        fmt.Println("JSON OBJECT :",shareUserData)

        userName := shareUserData["userName"].(string)
        halfKey := shareUserData["userPasswordKey"].(string)

        initVector:=shareUserData["initVector"].(string)

        fileName :=shareUserData["fileName"].(string)

        salt := shareUserData["salt"].(string)

        encryptedKey:= shareUserData["encryptedKey"].(string)

        encryptedFile:= shareUserData["encryptedFile"].(string)

        stmtIn,err:=database.Prepare("INSERT INTO ENDTOEND VALUES(?,?,?,?,?,?)")

        stmtIn.Exec(userName,halfKey,initVector,fileName,salt,encryptedKey)

        if _,err := os.Stat(userName); os.IsNotExist(err) {
            os.MkdirAll(userName,0777)
        }

        path := filepath.Join(userName,fileName)
       

        file,err := os.Create(path)

        checkError(err)

        defer file.Close()

        _,err = file.WriteString(encryptedFile)

        checkError(err)

}


func changePassword (conn net.Conn) {

    database,err :=sql.Open("mysql","krishnapt1251:password@/E2EE")
    if err != nil {
        panic(err.Error())
        }

    defer conn.Close()
    defer database.Close()

    var buf [2048] byte
    //var fileNameBuffer [512] byte

        // read upto 512 bytes

        //n,err :=conn.Read(fileNameBuffer[0:])

        n1, err := conn.Read(buf[0:])

        //fileName := string(fileNameBuffer[:n])
        //fileName+=".txt"

        s :=string(buf[:n1])

        fmt.Println(s)

        var shareUserData map[string]interface{}

        if err := json.Unmarshal([]byte(s),&shareUserData); err!=nil {
            panic(err)
        }

        fmt.Println("JSON OBJECT :",shareUserData)

        userName := shareUserData["userName"].(string)
        halfKey := shareUserData["userPasswordKey"].(string)

        initVector:=shareUserData["initVector"].(string)

        fileName :=shareUserData["fileName"].(string)

        salt := shareUserData["salt"].(string)

        encryptedKey:= shareUserData["encryptedKey"].(string)

        stmt,err := database.Prepare("DELETE FROM ENDTOEND WHERE USERNAME=? AND FILENAME=?")
        stmt.Exec(userName,fileName)


        stmtIn,err:=database.Prepare("INSERT INTO ENDTOEND VALUES(?,?,?,?,?,?)")

        stmtIn.Exec(userName,halfKey,initVector,fileName,salt,encryptedKey)

        checkError(err)

}

func registerUser(conn net.Conn) {
    database,err :=sql.Open("mysql","krishnapt1251:password@/E2EE")
    if err != nil {
        panic(err.Error())
        }

    defer conn.Close()
    defer database.Close()

    var buf[2048] byte

    n1, err := conn.Read(buf[0:])
    checkError(err)

    s :=string(buf[:n1])

    var userCredentials map[string]interface{}

    if err := json.Unmarshal([]byte(s),&userCredentials); err!=nil {
            panic(err)
        }

        userName:=userCredentials["userName"].(string)
        userKey:=userCredentials["userPassword"].(string)
        salt:=userCredentials["salt"].(string)

        stmt,err :=database.Prepare("INSERT INTO USER_CREDENTIALS VALUES(?,?,?)");
        stmt.Exec(userName,userKey,salt);
        checkError(err)
        fmt.Println("User Registered")
        fmt.Println("Registered Username:",userName)
}

func checkError(err error) {
    if err != nil {
        fmt.Fprintf(os.Stderr, "Fatal error: %s", err.Error())
        os.Exit(1)
    }
}

func main() {
    
    go createFilePort()
    go authorizeUserPort()
    go openFilePort()
    go openFilePort2()
    go shareFilePort()
    go changePasswordPort()
    go registerUserPort()
    
    var input string
    fmt.Scanln(&input)
    fmt.Println("done")
    fmt.Println("Server Running")

}

